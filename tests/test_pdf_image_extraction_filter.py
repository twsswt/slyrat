import unittest

from slirm.retrievers import StringRetriever
from slirm.filter import PDFImageExtractionFilter


class PDFImageExtractionFilterTestCase(unittest.TestCase):

    def setUp(self):

        entries = \
        """
            @ARTICLE{6740844, 
                local_file_path = {pdfs/6740844.pdf}
            }
        """

        string_retriever = StringRetriever(entries)
        self.pdf_content_filter = PDFImageExtractionFilter(string_retriever)

    def test_pdf_content_filter(self):

        result = self.pdf_content_filter.pull()
        self.assertEqual(1, len(result.get_entry_list()))


if __name__ == '__main__':
    unittest.main()
