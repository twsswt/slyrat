import unittest

from slirm.filter import PDFCacheFilter
from slirm.retrievers import StringRetriever


class PDFCacheFilterTestCase(unittest.TestCase):

    def test_pdf_cache_ieee_entry(self):

        ieee_entry = \
            """
                @ARTICLE{6740844, 
                    author={A. Zanella and N. Bui and A. Castellani and L. Vangelista and M. Zorzi}, 
                    journal={IEEE Internet of Things Journal}, 
                    title={Internet of Things for Smart Cities}, 
                    year={2014}, 
                    volume={1}, 
                    number={1}, 
                    pages={22-32}, 
                    keywords={Internet;Internet of Things;protocols;Padova smart city project;protocols;
                    advanced communication technology;value-addedservices;Smart City vision;urban IoT system;
                    link layer technology;digital services;heterogeneous end systems;Internet of Things;Urban areas;
                    Smart buildings;Monitoring;Smart homes;Business;IEEE 802.15 Standards;
                    Constrained Application Protocol (CoAP);Efficient XML Interchange (EXI);network architecture;
                    sensor system integration;service functions and management;Smart Cities;testbed and trials;6lowPAN}, 
                    doi={10.1109/JIOT.2014.2306328}, 
                    ISSN={2327-4662}, 
                    month={Feb},
                    article_number = {6740844}
                }
            """

        string_retriever = StringRetriever(ieee_entry)
        self.pdf_cache_filter = PDFCacheFilter(source_pipe=string_retriever, target_dir='./pdfs')
        result = self.pdf_cache_filter.pull()
        self.assertEqual(1, len(result.get_entry_list()))

    def test_pdf_cache_acm_entry(self):
        acm_entry = \
            """
             @article{Finerman:1969:EN:356540.356541,
                 author = {Finerman, Aaron},
                 title = {An Editorial Note},
                 journal = {ACM Comput. Surv.},
                 issue_date = {March 1969},
                 volume = {1},
                 number = {1},
                 month = mar,
                 year = {1969},
                 issn = {0360-0300},
                 pages = {1--},
                 url = {http://doi.acm.org/10.1145/356540.356541},
                 doi = {10.1145/356540.356541},
                 acmid = {356541},
                 publisher = {ACM},
                 address = {New York, NY, USA},
            } 
            """

        string_retriever = StringRetriever(acm_entry)
        self.pdf_cache_filter = PDFCacheFilter(source_pipe=string_retriever, target_dir='./pdfs')
        result = self.pdf_cache_filter.pull()
        self.assertEqual(1, len(result.get_entry_list()))



if __name__ == '__main__':
    unittest.main()
