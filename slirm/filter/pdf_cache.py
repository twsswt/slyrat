import os
import requests

from bibtexparser.bibdatabase import BibDatabase


def _default_file_name_format(entry):
    return entry['ID'].replace(':', '_') + ".pdf"


class PDFCacheFilter(object):

    def __init__(self, source_pipe, target_dir, file_name_format=_default_file_name_format):
        self.source_pipe = source_pipe
        self.target_dir = target_dir
        self.format_file_name = file_name_format

        self.cookies = {}

        user_agents = [
            'Mozilla/5.0 (Windows NT 6.1; WOW64)',
            'AppleWebKit/537.36 (KHTML, like Gecko)',
            'Chrome/35.0.1916.114 Safari/537.36']

        self.headers = {'User-Agent': " ".join(user_agents)}

        if not os.path.exists(self.target_dir):
            os.mkdir(self.target_dir)

    def pull(self):
        result = BibDatabase()

        for entry in self.source_pipe.pull().get_entry_list():
            if 'article_number' in entry:
                entry = self.cache_ieee_entry(entry)
            elif True:
                entry = self.cache_acm_entry(entry)

            if 'local_file_path' in entry:
                result.get_entry_list().append(entry)

        return result

    def cache_ieee_entry(self, entry):
        url_template = "http://ieeexplore.ieee.org/stampPDF/getPDF.jsp?tp=&isnumber=&arnumber=%s"
        article_number = entry['article_number']
        url = url_template % article_number
        return self.cache_from_url(url, entry)

    def cache_acm_entry(self, entry):
        url_template = "https://dl.acm.org/ft_gateway.cfm?id=%s"
        article_number = entry['doi'].split('.')[-1]
        url = url_template % article_number
        return self.cache_from_url(url, entry)

    def cache_springer_link_entry(self, entry):

        url_template = 'http://api.springernature.com/metadata/pam/doi/10.1007/s11276-008-0131-4'


    def cache_from_url(self, url, entry):

        response = requests.get(url, cookies=self.cookies, headers=self.headers)
        file_path = self.target_dir + "/" + self.format_file_name(entry)

        with open(file=file_path, mode='wb') as output_file:
            output_file.write(response.content)

        entry['local_file_path'] = file_path
        return entry




